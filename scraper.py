from urllib.request import urlopen, Request
import time
import re
import pickle

# List of the found indicators
# [ [Array to be built], [list of indicator identifiers] ]
indicators = [
    [[], ["abbreviation", "abbrev"]],
    [[], ["anagram", "anagr"]],
    [[], ["charade"]],
    [[], ["duplicate"]],
    [[], ["end"]],
    [[], ["even"]],
    [[], ["exampleof", "example"]],
    [[], ["first"]],
    [[], ["hidden"]],
    [[], ["homophone"]],
    [[], ["insertion", "inside", "envelope"]],
    [[], ["last"]],
    [[], ["middle", "outer"]],
    [[], ["odd"]],
    [[], ["reverse", "revers", "backward"]],
    [[], ["subtraction"]],
    ]

def scraper():
    # The pages to scrape clues from
    # categories = [["https://www.fifteensquared.net/category/everyman/", 40],
    #               ["https://www.fifteensquared.net/category/guardian/", 250]]

    categories = [["https://www.fifteensquared.net/category/everyman/", 1]]

    scrapePuzzlesTime = time.time()
    # Get all the pages from each category
    for cat in categories:
        puzzles = []
        reg_url = cat[0]
        for i in range(0, cat[1]):
            headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.3'}
            req = Request(url=reg_url, headers=headers)
            html = urlopen(req).read().decode("utf-8")

            # Get main body of fifteensquared.net
            main_start_index = html.find("<main id=\"main\" class=\"site-main\">")
            main_end_index   = html.find("</main>\n</div>")

            main_body = html[main_start_index:main_end_index]

            #Get location of the headers for each crossword listed
            headers     = [m.start() + 9 for m in re.finditer("<header class=\"entry-header\">", main_body)]
            headers_end = [m.start() for m in re.finditer("</header>", main_body)][1:]
            #Obtain the url for each puzzle
            for s, e in zip(headers, headers_end):
                c = main_body[s:e]
                url_s = c.find("<a href=\"")
                url_e = c.find("\" rel=\"bookmark\">")
                puzzles.append(c[url_s+9:url_e])

            next_page     = main_body.find("<a class=\"next page-numbers\" href=\"") + 35
            next_page_end = main_body.find("\">Next <span aria-hidden=\"true\">&rarr;</span></a></div> </nav>")

            reg_url = main_body[next_page:next_page_end]

        # Loop through all puzzles and obtain their clues
        for p in puzzles:
            scrape_page(p, reg_url)

    print(" - " + str(len(puzzles)) + " Puzzles Retrieved, Time taken: " + str((time.time() - scrapePuzzlesTime) / 60) + " minutes")

# [clue, desc, ans, defin]
allCluesAndDefs = []
def scrape_page(reg_url, initialPage):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.3'}
    req = Request(url=reg_url, headers=headers) 
    html = urlopen(req).read().decode("utf-8")

    #Get main body of puzzle page
    main_start_index = html.find("<main id=\"main\" class=\"site-main\">")
    main_end_index   = html.find("</main>\n</div>")

    #cluestart = max(html.find(">across<"), html.find(">Across<"),  html.find(">ACROSS<"))
    cluestart = html.lower().find(">across<")
    clueend   = html.find(initialPage)

    main_body = html[cluestart:clueend]

    if (main_body.find("</tr>") == -1):
        main_body = main_body[main_body.find("</p>") + 4:]
        headers     = [m.start() + 3 for m in re.finditer("<p", main_body)]
        headers_end = [m.start() for m in re.finditer("</p>", main_body)]
    else:
        main_body = main_body[main_body.find("</tr>") + 4:]
        headers     = [m.start() + 4 for m in re.finditer("<tr", main_body)]
        headers_end = [m.start() for m in re.finditer("</tr>", main_body)]

    clues = []
    for s, e in zip(headers, headers_end):
        c = main_body[s:e]
        #TODO: Find func to do this
        #print(c[c.find("<"):])
        c = c[c.find("<"):].replace('&#8217;', "'").replace('&#8216;', "'").replace('&#8220;', '"').replace('&#8221;', '"').replace('&#8230;', '...').replace('&amp', '&').replace('&lt;', '<').replace('&gt;', '>')
        
        # Identify definitions for the clue
        definitions = []
        if (main_body.find("definition\">") == -1):
            underlineStarts = [m.start() + 11 for m in re.finditer("underline\">", c)]
        else:
            underlineStarts = [m.start() + 12 for m in re.finditer("definition\">", c)]

        for u in underlineStarts:
            definition = c[u:]
            definitionEnd = definition.find("<")
            definition = definition[:definitionEnd]
            definitions.append(definition)
        if (definitions != []):
            c = c + "["
            for d in definitions:
                c = c + '"' + d + '", '
            c = c[:-2] + "]\n"
        clue = ""
        isClue = True
        for x in c:
            if (x == "<"):
                isClue = False
            if (x == ">"):
                isClue = True
            if isClue:
                clue = clue + x
        clue = clue.replace(">", '')
        clue = clue.replace('\n\n', '\n')
        # if (max(clue.find("down"), clue.find("Down"),  clue.find("DOWN")) == -1):
        if (clue.lower().find("down") == -1):
            clues.append(clue)

    if (main_body.find("</tr>") != -1):
        clueDefs = []
        clueIndex = -1
        for x in clues:
            if (x[:x.find('\n')].isnumeric()):
                clueIndex = clueIndex + 1
                clueDefs.append(x[x.find('\n')+1:])
            else:
                if (clueIndex != -1):
                    clueDefs[clueIndex] = clueDefs[clueIndex] + x[x.find('\n')+1:]
        clues = clueDefs

    for c in clues:
        clue = ""
        desc = ""
        ans  = ""
        defin = ""
        if (main_body.find("</tr>") == -1):
            clue = c[:c.find('\n')]
            c = c[c.find('\n')+1:]
            ans = c[:c.find('\n')]
            c = c[c.find('\n')+1:]
        else:
            ans = c[:c.find('\n')]
            c = c[c.find('\n')+1:]
            clue = c[:c.find('\n')]
            c = c[c.find('\n')+1:]

        defin = c[:c.find('\n')]
        c = c[c.find('\n')+1:]
        desc = c[:c.find('\n')]
        # print("Start")
        # print(clue)
        # print(desc)
        # print(ans)
        allCluesAndDefs.append([clue, desc, ans, defin])

def analyser(clues):
    analyserStart = time.time()

    breakdowns = []
    for c in clues:
        clue = c[0]
        desc = c[1]
        ans  = c[2]

        sharedWords = []
        looking = False
        curPhrase = ""
        #TODO: Cleanup word finder!
        for word in desc.split():
            if (word[0] == "'" or word[:2] == "('"):
                looking = True
            if (word[0] == "(" or word[0] == "["):
                looking = True

            if looking:
                curPhrase = curPhrase + word + " "

            if (word[-1] == "'"):
                if (clue.lower().find(curPhrase[1:-2].lower()) != -1):
                    sharedWords.append([desc.lower().find(curPhrase[1:-2].lower()), curPhrase[1:-2].lower()])
                curPhrase = ""
                looking = False
            if (word[-2:] == "')"):
                if (clue.lower().find(curPhrase[2:-3].lower()) != -1):
                    sharedWords.append([desc.lower().find(curPhrase[2:-3].lower()), curPhrase[2:-3].lower()])
                curPhrase = ""
                looking = False
            if (word[-2:] == "'." or word[-2:] == "',"):
                if (clue.lower().find(curPhrase[1:-3].lower()) != -1):
                    sharedWords.append([desc.lower().find(curPhrase[1:-3].lower()), curPhrase[1:-3].lower()])
                curPhrase = ""
                looking = False
            if (word[-1] == ")" or word[-1] == "]"):
                if (clue.lower().find(curPhrase[1:-2].lower()) != -1):
                    sharedWords.append([desc.lower().find(curPhrase[1:-2].lower()), curPhrase[1:-2].lower()])
                curPhrase = ""
                looking = False        

        #print(sharedWords)

        for ind in indicators:
            for i in ind[1]:
                if (desc.lower().find(i) != -1):
                    idx = desc.lower().find(i)
                    for w in sharedWords:
                        if (w[0] > idx and abs(w[0] - idx) < 30):
                            if (ind[0].count(w[1]) == 0):
                                ind[0].append(w[1])
                            break

    print(" - Analysing Complete, Time taken: " + str((time.time() - analyserStart) / 60) + " minutes")
    return 1

def comparer():
    comparingStart = time.time()
    results = []

    for i in indicators:
        ind = i[1][0]
        f = open ("indicators/new/" + ind + ".txt", "r")
        new = f.read().split('\n')
        f.close
        f = open ("indicators/current/" + ind + ".txt", "r")
        cur = f.read().split('\n')
        f.close

        compounds = []

        shared = []
        known = []
        discovered = []

        # Loop through the words discovered by the scraper
        for i in new:
            # Remove endspace
            i = i.rstrip()
            
            # Detect if word is a compound word to add to the list of compound words
            if ((' ' in i) == True):
                if (compounds.count(i) == 0):
                    compounds.append(i)

            # Detect if this is a new word not currently known
            if (cur.count(i) == 0):
                if (discovered.count(i) == 0):
                    discovered.append(i)
            else:
                if (shared.count(i) == 0):
                    shared.append(i)

        # Loop through the words already known
        for i in cur:
            # Remove endspace
            i = i.rstrip()

            # Detect if word is a compound word to add to the list of compound words
            if ((' ' in i) == True):
                if (compounds.count(i) == 0):
                    compounds.append(i)

            # Detect if this is a word we knew that was not discovered
            if (new.count(i) == 0):
                known.append(i)
            
        
        shared.sort()
        discovered.sort()
        known.sort()

        results.append([ind.upper() + ": ", len(discovered), len(known), len(shared)])
        f.close

        f = open ("indicators/compound/" + ind + ".txt", "w")
        for c in compounds:
            f.write(c + '\n')
        f.close

        f = open ("indicators/discovered/" + ind + ".txt", "w")
        for c in discovered:
            f.write(c + '\n')
        f.close

    f = open ("indicators/results.txt", "w")
    for c in results:
        f.write(c[0] + "\n    Newly Discovered: " + str(c[1]) + "\n        Undiscovered: " + str(c[2]) + "\n              Shared: " + str(c[3]) + "\n")
    f.close

    print(" - Comparing Complete, Time taken: " + str((time.time() - comparingStart) / 60) + " minutes")

startTime = time.time()
print("Starting Time: " + time.strftime('%H:%M:%S', time.localtime(startTime)))

# After an initial run, the following lines can be run to prevent needing to pull all clues from fifteensquared in the future
# Scrape the clues from fifteensquared and pull their description, answer and the clue itself
scraper()
# Write the data to a file to be retrieved and used again for analysis
with open('allClues.data', 'wb') as filehandle:
    pickle.dump(allCluesAndDefs, filehandle)

f = open ("definitions.txt", "w")
for c in allCluesAndDefs:
    ans = c[2]
    defin = c[3]
    # Ignore empty entries
    if (ans and defin):
        f.write("[\"" + ans + "\", " + str(defin) + ']\n')
f.close

# Retrieve the clues from the file
with open('allClues.data', 'rb') as filehandle:
    allCluesAndDefs = pickle.load(filehandle)

# Begin analysis on the clue definitions
analyser(allCluesAndDefs)

for ind in indicators:
    f = open ("indicators/new/" + ind[1][0] + ".txt", "w")
    for c in ind[0]:
        f.write(c + '\n')
    f.close

comparer()

timeTaken = time.time() - startTime
print("Time Taken: " + str(timeTaken / 60) + " minutes")
